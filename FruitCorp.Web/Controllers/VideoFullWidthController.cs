﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EPiServer;
using EPiServer.Core;
using EPiServer.Web;
using EPiServer.Web.Mvc;
using FruitCorp.Web.Models.Pages;
namespace FruitCorp.Web.Controllers
{
    public class VideoFullWidthController : PageController<VideoFullWidth>
    {
        public ActionResult Index(VideoFullWidth currentPage)
        {
            /* Implementation of action. You can create your own view model class that you pass to the view or
             * you can pass the page type for simpler templates */

            return View(currentPage);
        }
    }
}
