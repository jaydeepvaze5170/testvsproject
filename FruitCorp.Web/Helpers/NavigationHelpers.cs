﻿using System.Web.Mvc;
using EPiServer.Web.Mvc.Html;
using EPiServer.Core;
using EPiServer;
using EPiServer.ServiceLocation;
using EPiServer.Web.Routing;
using System.Linq;
using EPiServer.Filters;
using System.Collections.Generic;
namespace FruitCorp.Web.Helpers
{
    public static class NavigationHelpers
    {
        public static void RenderMainNavigation(this HtmlHelper html)
        {
            var writer = html.ViewContext.Writer;
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            var topLevelPages = contentLoader.GetChildren<PageData>(ContentReference.StartPage);
            topLevelPages = FilterForVisitor.Filter(topLevelPages)
                            .OfType<PageData>()
                            .Where(x => x.VisibleInMenu);
            var contentLink = html.ViewContext.RequestContext.GetContentLink();
            //Top level elements
            writer.WriteLine("<nav class=\"navbar navbar-inverse\">");
            writer.WriteLine("<ul class=\"nav navbar-nav\">");
            if (ContentReference.StartPage.CompareToIgnoreWorkID(contentLink))
            {
                writer.WriteLine("<li class=\"active\">");
            }
            else
            {
                writer.WriteLine("<li>");
            }
            writer.WriteLine(html.PageLink(ContentReference.StartPage).ToHtmlString());
            writer.WriteLine("</li>");
            var currentBranch = contentLoader.GetAncestors(contentLink)
                                .Select(x => x.ContentLink)
                                .ToList();
            currentBranch.Add(contentLink);

            foreach (var topLevelPage in topLevelPages)
            {
                if (currentBranch.Any(x =>
                x.CompareToIgnoreWorkID(topLevelPage.ContentLink)))
                {
                    writer.WriteLine("<li class=\"active\">");
                }
                else
                {
                    writer.WriteLine("<li>");
                }
                writer.WriteLine(html.PageLink(topLevelPage).ToHtmlString());
                writer.WriteLine("</li>");
            }
            
            writer.WriteLine("</ul>");
            writer.WriteLine("</nav>");        
        }
        public static void RenderSubNavigation(this HtmlHelper html)
        {
            var contentLink = html.ViewContext.RequestContext.GetContentLink();
            var contentLoader = ServiceLocator.Current.GetInstance<IContentLoader>();
            var path = contentLoader.GetAncestors(contentLink).Reverse()
                        .SkipWhile(x =>
                        ContentReference.IsNullOrEmpty(x.ParentLink)
                        || !x.ParentLink.CompareToIgnoreWorkID(ContentReference.StartPage))
                        .OfType<PageData>()
                        .Select(x => x.PageLink)
                        .ToList();
            var currentPage = contentLoader.Get<IContent>(contentLink) as PageData;
            if (currentPage != null)
            {
                path.Add(currentPage.PageLink);
            }
            var root = path.FirstOrDefault();
            if (root == null)
            { return; }
            RenderSubNavigationLevel(html,root,path,contentLoader);
        } 
        private static void RenderSubNavigationLevel(HtmlHelper helper,ContentReference levelRootLink,IEnumerable<ContentReference> path,IContentLoader contentLoader)
        {
            //Retrieve and filter the pages on the current level
            var children = contentLoader.GetChildren<PageData>(levelRootLink);
            children = FilterForVisitor.Filter(children)
            .OfType<PageData>()
            .Where(x => x.VisibleInMenu);

            if (!children.Any())
            {
                //There's nothing to render on this level so we abort
                //in order not to write an empty ul element.
                return;
            }

            var writer = helper.ViewContext.Writer;

            //Open list element for the current level
            writer.WriteLine("<ul class=\"nav\">");

            //Project to an anonymous class in order to know
            //the index of each page in the collection when
            //iterating over it.
            var indexedChildren = children
            .Select((page, index) => new {index, page})
            .ToList();

            foreach (var levelItem in indexedChildren)
            {
                var page = levelItem.page;
                var partOfCurrentBranch = path.Any(x =>
                x.CompareToIgnoreWorkID(levelItem.page.ContentLink));

                if (partOfCurrentBranch)
                {
                    //We highlight pages that are part of the current branch,
                    //including the currently viewed page.
                    writer.WriteLine("<li class=\"active\">");
                }
                else
                {
                    writer.WriteLine("<li>");
                }
                writer.WriteLine(helper.PageLink(page).ToHtmlString());

                if (partOfCurrentBranch)
                {
                    //The page is part of the current pages branch,
                    //so we render a level below it
                    RenderSubNavigationLevel(
                    helper,
                    page.ContentLink,
                    path,
                    contentLoader);
                }
                writer.WriteLine("</li>");
            }
            //Close list element
            writer.WriteLine("</ul>");
        }
    }
    
}