﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web;

namespace FruitCorp.Web.Models.Media
{
    [ContentType(DisplayName = "VideoFile", GUID = "d32128a6-d12f-4489-9ed8-be41da58fa9d", Description = "")]
    [MediaDescriptor(ExtensionString = "flv,mp4,webm")]
    public class VideoFile : VideoData
    {
        
                [CultureSpecific]
                [Editable(true)]
                [Display(
                    Name = "Description",
                    Description = "Description field's description",
                    GroupName = SystemTabNames.Content,
                    Order = 1)]
                public virtual string Description { get; set; }

                [UIHint(UIHint.Image)]
                public virtual ContentReference PreviewImage { get; set; }
         
    }
}