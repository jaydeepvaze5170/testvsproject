﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;

namespace FruitCorp.Web.Models.Blocks
{
    [ContentType(DisplayName = "TemplateITBlock", GUID = "396462e8-0edf-4a7b-8cdf-fb2bd3c83145", Description = "")]
    public class TemplateITBlock : BlockData
    {
        /*
                [CultureSpecific]
                [Display(
                    Name = "Name",
                    Description = "Name field's description",
                    GroupName = SystemTabNames.Content,
                    Order = 1)]
                public virtual string Name { get; set; }
         */
    }
}