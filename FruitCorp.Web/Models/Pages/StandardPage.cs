﻿using EPiServer.Core;
using EPiServer.DataAnnotations;

namespace FruitCorp.Web.Models.Pages
{
    [ContentType(DisplayName = "StandardPage", GUID = "36e6789a-84f8-4883-91e8-9fa31818977e", Description = "")]
    public class StandardPage : PageData
    {
        /*
                [CultureSpecific]
                [Display(
                    Name = "Main body",
                    Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
                    GroupName = SystemTabNames.Content,
                    Order = 1)]
                public virtual XhtmlString MainBody { get; set; }
         */
        public virtual string MainIntro { get; set; }
        public virtual XhtmlString MainBody { get; set; }
        public virtual ContentArea MainContentArea { get; set; }
    }
}