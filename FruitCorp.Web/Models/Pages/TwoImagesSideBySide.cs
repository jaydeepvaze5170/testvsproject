﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
namespace FruitCorp.Web.Models.Pages
{
    [ContentType(DisplayName = "TwoImagesSideBySide", GUID = "fa6ecd58-78cb-4a64-afbf-c8b4cdc5057b", Description = "")]
    public class TwoImagesSideBySide : PageData
    {
        [CultureSpecific]
        [Display(
            Name = "Image",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        [UIHint(UIHint.Image)]
        [ImageDescriptor(Width = 300, Height = 247)]
        public virtual ContentReference FirstImage { get; set; }

        [CultureSpecific]
        [Display(
            Name = "Image",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 2)]
        [UIHint(UIHint.Image)]
        [ImageDescriptor(Width = 300, Height = 247)]
        public virtual ContentReference SecondImage { get; set; }    
    }
}