﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web;
namespace FruitCorp.Web.Models.Pages
{
    [ContentType(DisplayName = "ImageWithTxtOnRight", GUID = "23f72840-7214-4853-8818-abba4c703189", Description = "")]
    public class ImageWithTxtOnRight : PageData
    {
        [CultureSpecific]
        [Display(
            Name = "Image",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        [UIHint(UIHint.Image)]
        [ImageDescriptor(Width = 300, Height = 247)]
        public virtual ContentReference Image { get; set; }    
    
        [CultureSpecific]
        [Display(
            Name = "Main body",
            Description = "The main body will be shown in the main content area of the page, using the XHTML-editor you can insert for example text, images and tables.",
            GroupName = SystemTabNames.Content,
            Order = 2)]
        public virtual XhtmlString MainBody { get; set; }
        
    }
}