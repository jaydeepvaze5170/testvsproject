﻿using System;
using System.ComponentModel.DataAnnotations;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.DataAnnotations;
using EPiServer.SpecializedProperties;
using EPiServer.Web;

namespace FruitCorp.Web.Models.Pages
{
    [ContentType(DisplayName = "VideoFullWidth", GUID = "c5dd3d19-b23a-46e7-9a33-0969cbbeccf1", Description = "")]
    public class VideoFullWidth : PageData
    {
        [CultureSpecific]
        [Display(
            Name = "Video",
            Description = "",
            GroupName = SystemTabNames.Content,
            Order = 1)]
        [UIHint(UIHint.Video)]
        //[ImageDescriptor(Width = 300, Height = 247)]
        public virtual ContentReference MyVideo { get; set; }    
    }
}